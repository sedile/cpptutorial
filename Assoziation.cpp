#include <iostream>
#include "Assoziation.h"

/* Ein namensraum wird mit namensraum::inhalt angesprochen oder
 * man nimmt using namespace namensraum */
using namespace ns;

Zentrum::Zentrum() {}

Zentrum::~Zentrum() {}

void Zentrum::start() {
	Zentrum zentrum;

	/* Unidirektional 1 zu 1 : Nur Zentrum kennt Nord*/
	Nord nord;
	nord.drucke();

	std::cout << "-----------------------------" << std::endl;

	/* Bidirektional 1 zu 1 : Zentrum kennt Ost und Ost kennt Zentrum */
	Ost ost;
	ost.verbindeMitZentrum(zentrum);
	ost.drucke();

	std::cout << "-----------------------------" << std::endl;

	/* Unidirektional 1 zu n : Nur Zentrum kennt alle Nord*/
	Sued sued[3];
	for (int i = 0; i < 3; i++) {
		sued[i].drucke();
	}

	std::cout << "-----------------------------" << std::endl;

	/* Bidirektional 1 zu n : Zentrum kennt alle West und jeder West kennt sein Zentrum */
	West west[3];
	for (int i = 0; i < 3; i++) {
		west[i].verbindeMitZentrum(zentrum);
		west[i].drucke();
	}
}

void Zentrum::druckeOstZentrum() {
	std::cout << "ZENTRUM : 1 zu 1 Bidirektional" << std::endl;
}

void Zentrum::druckeWestZentrum() {
	std::cout << "ZENTRUM : 1 zu n Bidirektional" << std::endl;
}

Nord::Nord() {}

Nord::~Nord() {}

void Nord::drucke() {
	std::cout << "NORD : 1 zu 1 Unidirektional" << std::endl;
}

Ost::Ost() : _zentrum() {}

Ost::~Ost() {}

void Ost::verbindeMitZentrum(Zentrum &zentrum) {
	_zentrum = zentrum;
}

void Ost::drucke() {
	std::cout << "OST : 1 zu 1 Bidirektional" << std::endl;
	_zentrum.druckeOstZentrum();
}

Sued::Sued() {}

Sued::~Sued() {}

void Sued::drucke() {
	std::cout << "SUED : 1 zu n Unidirektional" << std::endl;
}

West::West() : _zentrum() {}

West::~West() {}

void West::verbindeMitZentrum(Zentrum &zentrum) {
	_zentrum = zentrum;
}

void West::drucke() {
	std::cout << "WEST : 1 zu n Bidirektional" << std::endl;
	_zentrum.druckeWestZentrum();
}
