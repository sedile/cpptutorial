#include <iostream>

/* Ein Namensraum dient der Sammlung von Klassen, Funktionen, Variablen, ...,
 * um sich z.B. ein Modul/Bibliothek zu bauen, wie 'std' */
namespace ns {

    /* Innerhalb eines Namensraum kann man theoretisch auch mehrere definieren */
    namespace ns_inner {

        using namespace std;

        void print(){
            cout << "Das geht ja" << "\n";
        }

    }

    struct Zentrum {
        explicit Zentrum();
        virtual ~Zentrum();
        void start();
        void druckeOstZentrum();
        void druckeWestZentrum();
    };

    class Nord {
    public:
        explicit Nord();
        ~Nord();
        void drucke();
    };

    class Ost {
    public:
        explicit Ost();
        ~Ost();
        void verbindeMitZentrum(Zentrum&);
        void drucke();
    private:
        Zentrum _zentrum;
    };

    class Sued {
    public:
        explicit Sued();
        ~Sued();
        void drucke();
    };

    class West {
    public:
        explicit West();
        ~West();
        void verbindeMitZentrum(Zentrum&);
        void drucke();
    private:
        Zentrum _zentrum;
    };
}
