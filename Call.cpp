#include <iostream>
#include "Call.h"

void Call::callX() {
    short test = 0;
    std::cout << "Beginn : " << test << std::endl;
    callByValue(test);
    std::cout << "Nach Call-by-Value : " << test << std::endl;
    callByReference(test);
    std::cout << "Nach Call-by-Refernce : " << test << std::endl;
    short *p = &test;
    p = pointerUebergabe(p);
    std::cout << "Nach Pointerveraenderung : " << *p << std::endl;
    std::cout << "Adresse eines Pointers : " << p << std::endl;
    std::cout << "Vor Pointeruebergabe ohne Rueckgabe : " << test << std::endl;
    pointerUebergabeVoid(&test);
    std::cout << "Nach Pointerveraenderung ohne Rueckgabe : " << test << std::endl;
    p = nullptr;
    short *w = pointerRueckgabe();
    std::cout << "Pointererzeugung mit Pointerrueckgabe : " << *w << std::endl;
    delete w;

    unsigned short a = 5;
    const int b = 10;
    long erg = 0;
    int maximum = 3;
    int *pC = new int[maximum];
    *(pC) = 13;
    *(pC + 1) = 6;
    *(pC + 2) = 3;
    erg = komplexDemo(a,b,erg,pC,maximum);
    std::cout << "Ergebnis der 'komplexen Funktion' : " << erg << std::endl;
    delete[] pC;
    pC = nullptr;
}

/* Hier wird die Ã¼bergebene Variable kopiert und auf die Kopie gearbeitet */
void Call::callByValue(short wert) {
    wert++;
}

/* Hier wird auf der Variablen selber gearbeitet und 'Ã¼berlebt' somit die Funktion.
Dies sollte genutzt werden wenn Variablenwerte verÃ¤ndert werden sollen */
void Call::callByReference(short &wert) {
    wert++;

    /* Ein Array eine Referrenzvariable Ã¼bergeben */
    short feld[2];
    *(feld) = wert;
    *(feld + 1) = wert + 7;
    //	std::cout << feld[1] << std::endl;

}

/* Hier wird auf der Variablen selber gearbeitet und 'Ã¼berlebt' somit die Funktion.
Mit const wird sichergestellt, dass die original uebergebene Variable nicht
ueberschrieben wird */
void Call::callByReferenceConst(const short &wert) {
    short a = 5 + wert;
    a++;
}

/* Hier wird ein Pointer auf eine Variable Ã¼bergeben und wieder zurÃ¼ckgegeben. Die Variable
verÃ¤ndert sich, da der Pointer den Wert der Variable verÃ¤ndert. Funktionen wo ein Pointer
erzeugt wird sollte diesen auch zurÃ¼ckgeben, also kein void. Ausserdem sollte man darauf
achten, dass ein Pointer der nicht mehr benoetigt wird auf "nullptr" gesetzt wird.*/
short* Call::pointerUebergabe(short *wert) {
    if (wert == nullptr) {
        return nullptr;
    }

    *wert = *wert + 1;

    /* Ein Array ein Zeigerwert Ã¼bergeben */
    short feld[2];
    feld[0] = *wert;
    feld[1] = (*wert + 7);

    /* Zum testen */
    std::cout << "Pointermodifikation : " << *(feld+1) << std::endl;

    return wert;
}

/* Hier wird ein Pointer in der Funktion erzeugt und zurueckgegeben */
short* Call::pointerRueckgabe() {
    short *ptr = new short;
    *ptr = -1;
    return ptr;
}

/* Hier wird ein Pointer auf eine Variable Ã¼bergeben und verÃ¤ndert. Wird ein Pointer uebergeben
so kann dieser direkt in der Funktion bearbeitet werden, ohne diesen zurÃ¼ckgeben zu mÃ¼ssen.
i.d.R. werden Pointer auf Objekte gesetzt, also z.B. Objekt *objekt */
void Call::pointerUebergabeVoid(short *wert) {
    if (wert == nullptr) {
        return;
    }
    *wert = *wert + 1;
}

/* Ein demonstratives Beispiel mit diversen Arten der Ãbergabeparametern */
long& Call::komplexDemo(unsigned short zahl, const int &P, long &ergebnis, int *args, int maximum){
    if ( args == nullptr){
        return ergebnis;
    }

    ergebnis = static_cast<long>(zahl * P);
    for(int i = 0; i < maximum; i++){
        ergebnis -= static_cast<long>(*(args + i));
    }
    return ergebnis;
}
