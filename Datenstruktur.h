#include <iostream>

/* "typedef" : Eigene Datentypen können definiert werden, oder bestehende umbenannt werden */
typedef unsigned short natzahl;

struct Datenstruktur {
	void auswahl();
	void feld();
	void vektor();
	void einfachListe();
	void set();
	void map();
};
