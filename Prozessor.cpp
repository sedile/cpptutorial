#include <iostream>

/* PrÃ¤prozessor "ersetzt" diese Werte im Code */
#define MAX 10
#define mult(x,y) (x)*(y)

/* Bedingte Kompilierung, wenn ein "Codefragment" ausgefÃ¼hrt werden soll */
#define NORMAL
// #define SPEZIAL

/* Error, wenn zwei Bedingte Anweisungen zugleich aktiv sind, siehe oben */
#ifdef NORMAL
#ifdef SPEZIAL
#error "NORMAL und SPEZIAL sind aktiv"
#endif
#endif

void definiert() {
    int feld[MAX];
    for (int i = 0; i < MAX; i++) {
        feld[i] = mult(i, i);
    }

    for (int j = 0; j < MAX; j++) {
        std::cout << feld[j] << std::endl;
    }
}

/* #undef, wenn ein #define-Wert sehr kurzzeitig existieren soll */
void undefiniert(int x) {
#define X 1
    int erg = x * X;
    std::cout << erg << std::endl;
#undef X
}

void funktion() {
    std::cout << "Bedingte Kompilierung, da #define NORMAL" << std::endl;
}

void funktion(int x) {
    std::cout << "Bedingte Kompilierung, da #define SPEZIAL " << x << std::endl;
}

class Prozessor {
public:
    void definiert();
    void undefiniert(int);
private:
#ifdef NORMAL
    void funktion(); // Wenn NORMAL dann fÃ¼hre diese Funktion aus
#endif

#ifdef SPEZIAL
    void funktion(int); // Wenn SPEZIAL dann fÃ¼hre diese Funktion aus
#endif
};

