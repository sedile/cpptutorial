#ifndef SORT_H
#define SORT_H

typedef unsigned short num;

class Sort {
private:
    void merge(num *arr, num links, num mitte, num rechts);
public:
    explicit Sort();
    virtual ~Sort();

    void bubbleSort();
    void insertionSort();
    void quickSort(num *arr, num links, num rechts);
    void mergeSort(num *arr, num links, num rechts);
};

#endif // SORT_H
