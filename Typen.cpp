//	Ein Union sucht zunaechst den groessten Datentypen der innerhalb des Union-Scopes vorkommt.
//	Danach wird entsprechend viel RAM fuer den groessten Datentypen reserviert. Nun sorgt der
//	Union dafuer, dass alle Datentypen die sich innerhalb des Unions befinden dafuer, dass sie
//	sich alle den gleichen RAM-Block teilen. Veraenderungen an ein Datentyp hat auswirkungen
//	auf moeglicherweise alle anderen Datentypen innerhalb des Union-Scopes
/*
union Farbe {
    struct {
        unsigned char a, b, g, r;
    };
    unsigned int farbcode;
    }; */

//	Ein enum ist ein Aufzaehlungstyp
/*
enum Ampelzustand { ROT, GELB, GRUEN, ROTGELB, ACHTUNG, AUS }; */

// Union ansprechen
/*
Farbe farbe;
Farbe farbe;
farbe.farbcode = 0xC5FA7BC1;
std::cout << +farbe.r << " " << +farbe.g << " " << +farbe.b << " " << +farbe.a << std::endl;

// Enum ansprechen
Ampelzustand aRot = ROT; */

// static in Funktionen
/*
void func(){
  static int x = 0;
  x++;
  std::cout << x << std::endl;
}

statische Variablen in Funktionen Ã¼berleben das terminieren der Funktion und kÃ¶nnen nur innerhalb
dieser Funktion benutzt werden
*/
