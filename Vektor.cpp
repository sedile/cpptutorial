#include "Vektor.h"

Vektor::Vektor()
    : _x(0), _y(0)
{ }

Vektor::Vektor(short x, short y)
    : _x(x), _y(y)
{ }

Vektor::Vektor(const Vektor &cpy)
    : _x(cpy.getX()), _y(cpy.getY())
{ }

Vektor::Vektor(Vektor &&obj)
    : _x(obj.getX()), _y(obj.getY()) {
    // delete data; falls eine heapvariable in dieser Klasse vorhanden ist;
    // data = obj.data; wert von obj zuweisen
}

Vektor::~Vektor() { }

Vektor& Vektor::operator =(const Vektor &obj){
    if ( &obj == this){
        return *this;
    } else {
        _x = obj.getX();
        _y = obj.getY();
        return *this;
    }
}

Vektor& Vektor::operator =(Vektor &&obj){
    if ( &obj == this){
        return *this;
    } else {
        _x = obj.getX();
        _y = obj.getY();
        return *this;

        // nur bei einer heapvariable
        // delete data;
        // data = obj.data;
        // obj.data = nullptr;
    }
}

void Vektor::setX(short x){
    _x = x;
}

void Vektor::setY(short y){
    _y = y;
}

short Vektor::getX() const {
    return _x;
}

short Vektor::getY() const {
    return _y;
}

Vektor& Vektor::operator +=(const Vektor &obj){
    _x += obj.getX();
    _y += obj.getY();
    return *this;
}

Vektor& Vektor::operator -=(const Vektor &obj){
    _x -= obj.getX();
    _y -= obj.getY();
    return *this;
}

Vektor& Vektor::operator *=(const short skalar){
    _x *= skalar;
    _y += skalar;
    return *this;
}

Vektor Vektor::operator +(const Vektor &obj){
    return Vektor(_x + obj._x, _y + obj._y);
}

std::ostream& operator <<(std::ostream &os, const Vektor &obj){
    os << "(" << obj.getX() << " " << obj.getY() << ")";
    return os;
}