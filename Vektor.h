#ifndef VEKTOR_H
#define VEKTOR_H

#include <ostream>

class Vektor {
private:
    short _x, _y;
public:
    /* Default-Konstruktor */
    explicit Vektor();

    /* Parametrisierter-Konstruktor
     * ein ueberladener Konstruktor */
    explicit Vektor(short x, short y);

    /* Copy-Konstruktor
     * wird aufgerufen wenn ein Objekt per Kopie zurueckgegeben wird oder
     * das Objekt als Kopie an eine Funktion uebergeben wird */
    Vektor(const Vektor &cpy);

    /* Move-Konstruktor
     * benutzt Werte des uebergebenen Typs ohne die komplette Instanz
     * zu kopieren */
    Vektor(Vektor &&obj);

    /* Destruktor : Wenn man mit ein Heap arbeitet */
    virtual ~Vektor();

    /* Assignment-Operator */
    Vektor& operator=(const Vektor &obj);

    /* Move-Assignment-Operator */
    Vektor& operator=(Vektor &&obj);

    /* Operatoren koennen ueberladen werden, um z.B.
     * mit 'Objekten' rechnen zu koennen. Wird der eigene
       Datentyp veraendert, so wird eine Referenz zurueckgegeben und
       keine Kopie erzeugt */
    Vektor& operator+=(const Vektor &obj);
    Vektor& operator-=(const Vektor &obj);
    Vektor& operator*=(const short skalar);

    /* Wird ein neuer Datentyp erzeugt, so wird eine Kopie
     * zurueckgegeben */
    Vektor operator+(const Vektor &obj);

    /* Setterfunktionen */
    void setX(short x);
    void setY(short y);

    /* Getterfunktionen */
    short getX() const;
    short getY() const;
};

std::ostream& operator<<(std::ostream &os, const Vektor &obj);

#endif // VEKTOR_H