#include "Zeiger.h"
#include <iostream>

void Zeiger::pointer(){
    /* Erzeuge ein Pointer */
    Vektor *vektor = new Vektor;
    std::cout << vektor;

    /* Gebe den mit new erzeugten Speicher wieder frei,
     * und setzte den Pointer auf null */
    delete vektor;
    vektor = nullptr;
}

void Zeiger::sharedpointer(){
    /* Ein sharedpointer hat ein Referenzzaehler der Zaehlt
     * wieviele sharedpointer auf dieses Objekt zeigen. Der
     * Speicher wird dann erst automatisch freigegeben, wenn
     * der Referenzzaehler den Wert 0 erreicht hat */
    std::shared_ptr<Vektor> vektor(std::make_shared<Vektor>(1,1));

    /* Demonstration, dass mehrere sharedpointer auf das obige Objekt zeigen koennen */
    {
        std::shared_ptr<Vektor> innerPtr(vektor);
        std::cout << "Pointercounter : " << innerPtr.use_count() << "\n";
    }
    /* ... und wie der Counter beim verlassen eines Scopes dekrementiert wird */
    std::cout << "Pointercounter : " << vektor.use_count() << "\n";
}

void Zeiger::uniquepointer(){
    /* Ein uniquepointer ist wie der sharedpointer mit den
     * unterschied, dass nur ein einziger pointer. nÃ¤mlich
     * der uniquepointer auf das Objekt zeigt */
    std::unique_ptr<Vektor> vektor(std::make_unique<Vektor>(2, 2));
    uniqueParameter(vektor);
}

void Zeiger::uniqueParameter(std::unique_ptr<Vektor> &sp){
    /* Uniquepointer per Referenz Ã¼bergeben */
    std::cout << sp.get();
}

void Zeiger::zeigerwerte(){
    unsigned short s = 5;

    /* Referenzieren : Ein Pointer zeigt auf die Speicherzelle von s*/
    unsigned short *sP = &s;
    std::cout << "Referenzieren : "<<*sP << std::endl;

    /* Dereferenzieren : Pointerinhalt wird in eine Variable gespeichert */
    unsigned short t = *sP;
    std::cout << "Dereferenzieren : "<< t << std::endl;
}

void Zeiger::feldbeispiel(){
    int feld[] = {1,2,3,4,5,6,7,8,9};

    /* Teile die groesse des Feldes durch das erste Element, um so
     * die Anzahl der Feldelemente zu erhalten
     * erstes Element : Datentyp */
    int n = sizeof(feld) / sizeof(feld[0]);
    int *summe = felddurchlauf(feld,n);
    std::cout << "Summe : " << *summe << std::endl;
    delete summe;
    summe = nullptr;
}

int* Zeiger::felddurchlauf(const int *feld, const int elem){
    int *summe = new int;
    *summe = 0;
    for(int i = 0; i < elem; i++){
        *summe += feld[i];
    }
    return summe;
}

int Zeiger::functionPointerTestOne(int x, int y){
    return x + y;
}

int Zeiger::functionPointerTestTwo(int x, int y){
    return x - y;
}

void Zeiger::functionPointer(){
    /* Funktionenzeigerdefinition */
    int (Zeiger::*fkt)(int,int) = nullptr;

    /* Funktionenzeiger anwenden */
    fkt = &Zeiger::functionPointerTestOne;
    int erg = (*this.*fkt)(4, 6);
    std::cout << "Funktionszeigertest Eins : " << erg << std::endl;

    /* neue Zuweisung */
    fkt = &Zeiger::functionPointerTestTwo;
    erg = (*this.*fkt)(4, 6);

    std::cout << "Funktionszeigertest Zwei : " << erg << std::endl;
}

void Zeiger::zeigerOhneNew(){
    Vektor vektor(1,2);

    /* Pointer auf eine Variable */
    Vektor *pVektor = getVektor(vektor);
    std::cout << "Originale Vektor-Adresse : " << &vektor << "\n";
    std::cout << "Pointer auf Variable : " << &pVektor << "\n";
    pVektor = nullptr;
}

Vektor* Zeiger::getVektor(Vektor &vektor){
    /* Darf man nur, wenn ein Objekt vor Funktionsaufruf schon existiert */
    return &vektor;
}
