#ifndef ZEIGER_H
#define ZEIGER_H

#include <memory>

#include "Vektor.h"

struct Zeiger {
    void pointer();
    void sharedpointer();
    void uniquepointer();
    void uniqueParameter(std::unique_ptr<Vektor> &sp);
    void zeigerwerte();
    void feldbeispiel();
    int* felddurchlauf(const int *feld, const int elemente);
    int functionPointerTestOne(int,int);
    int functionPointerTestTwo(int,int);
    void functionPointer();

    void zeigerOhneNew();
    Vektor* getVektor(Vektor &vektor);
};

#endif // ZEIGER_H
